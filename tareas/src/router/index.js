import Vue from 'vue'
import Router from 'vue-router'
import Inicio from '@/components/Inicio'
import DataTarea from '@/components/dataTarea'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Inicio',
      component: Inicio
    },
    {
      path: '/listadoTareas/:id',
      name: 'DataTarea',
      component: DataTarea
    },
    {path: '*', redirect: '/'}
  ]
})
